# Phonotactics

## Syllable structure

The common syllable structure in Ravna is CV(C).

The final consonants cannot be glottal.

## Other constraints

The vowel /ɨ/ can only occur where the syllable constraint forbid it to
disappears.  For example, <i>Ravna</i> is actually <i>Ravina</i>, but since
<i>i</i> is unstressed, it disappears.

Final consonants are devoiced, unless there is no voiceless equivalent for the
consonant.  <i>Ravna</i> is pronounced [ˈraːfna] but <i>Ravino</i> is /raˈviːno/.

Glottal stop /ʔ/ also disappears when a compound/suffixed word forms with two
same vowels colliding, or when a consonant is followed by a vowel.

Example:

<i>ravi</i> (root) /ˈraːvɨ/ + <i>ino</i> /ʔino/ (human, person, people)
→ <i>Ravino</i> /raˈviːno/

(not <i>*Ravi'ino</i> /ˈraːvɨʔino/)
