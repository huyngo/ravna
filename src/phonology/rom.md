# Romanization

For the sake of convenience, Latin script is used to transcribe Ravna, as its
orthography is not convenience for displaying on computer.  It uses as few
diacritics as possible for the convenience of typing, with the exception of
umlaut vowels.

| Letter | Phoneme |
|--------|---------|
| a      | a       |
| ä, ae  | ɛ       |
| b      | b       |
| c      | k       |
| ch     | c       |
| d      | d       |
| e      | e, ə    |
| f      | f       |
| g      | g       |
| h      | h, ç    |
| i      | i, ɨ    |
| k      | x       |
| l      | l       |
| m      | m       |
| n      | n       |
| ñ, nj  | ɲ       |
| ŋ, nq  | ŋ       |
| o      | o       |
| ö, oe  | u       |
| oo     | ɔ       |
| p      | p       |
| r      | r       |
| s      | s       |
| t      | t       |
| v      | v       |
| y      | j       |
| z      | z       |

Notes: it is strongly preferred to use diacritics for handwritten
transcription.
