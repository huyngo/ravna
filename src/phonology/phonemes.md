# Phonemes

## Consonants

|           | labial | alveolar | palatal | velar | glottal |
|-----------|--------|----------|---------|-------|---------|
| nasal     | m      | n        | ɲ       | ŋ     |         |
| plosive   | p b    | t d      | c       | k g   | ʔ       |
| fricative | f v    | s z      | (ç) j   | x     | h       |
| liquid    |        | l r      |         |       |         |

Ravna consists of 22 standard consonants.

- /t/, /d/, and /c/ are aspirated [tʰ], [dʰ], [cʰ] in Ravin dialect, but
  unaspirated elsewhere
- /x/ can be realized as [χ] in some dialects
- [ç] is realization of /h/ at coda, which can be realized as [ʂ] in some rare
  dialects or by non-native speakers
- /s/ and /z/ is realized as [ʂ] and [ʐ] in Ravin dialect, but not others
- Some dialects don't have phoneme /ʔ/
- In modern Ravna dialect that Ravin people speak, /l/ and /r/ are merged to [ɺ]
- /r/ can be either approximant [ɹ], tap [ɾ], or trill [r] in other dialects

## Vowels

|        | front | central | back |
|--------|-------|---------|------|
| open   | i     | (ɨ)     | u    |
| mid    | e ɛ   | (ə)     | o    |
| closed | a     |         | ɔ    |

Ravna consists of 9 vowels.

- /ə/ occurs as unstressed /e/
- /ɨ/ occurs as unstressed /i/, which disappears when possible
- /a/ and /o/ have umlauts /ɛ/ and /u/ respectively.
- In verb conjugation ending, /e/ is realized as [ɛ] instead of [ə]. Similarly,
    /i/ is realized as [i] instead of [ɨ]

## Umlaut

Umlauts are the vowels that are modified when forming inflected forms.  These
vowels are the stressed vowels of the main root, which is usually the last
root.

## Stress

Ravna is a stress-accent language with clear pitch accent patterns.

If the word has 3 syllables or less:

- The primary stress is on the first syllable.
- The low pitch is on the first syllable and the high pitch is on the last
    syllable.  All other syllables have low pitches.

If the word has more than 3 syllables:

- The primary stress is on the first syllable.
- The secondary stress is on the third syllable.
- The high pitches are on the stresses, and low pitches are on other syllable.

These rules don't apply for prefixes—prefixes are never stressed.  For
prefixed words, stress rules apply starting from the main stem.  The pitches
are also reversed for these words

Another exception in compound:

- Compound with colliding vowels have the syllable with colliding vowels as
    primary stress, and the first syllable being secondary stress if there are
    more than two syllables.
- Compound with more than four syllables has the first syllable of the first
    root as primary stress and the first syllable of the second root as
    secondary stress.
