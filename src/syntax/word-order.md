# Word order

## Sentences

Sentences in Ravna are best not analyzed in terms of subjects and objects, but
topic and comment.  The sentence order is topic-comment-verb, which might
appears as SOV or OSV if one uses subject-object model.  When the comment
includes both a subject and object, however, the subjects mostly comes first.

## Modifiers

Modifiers (adjectives, adverbs) follow the words they modify.

## Adverbials

While the rule for modifiers extends to spatial adverbial phrases, it should be
noted that destination and origin are *before* a verb.  Some examples for this
rule:

| Ravna | English |
|-------|---------|
|  | I ran *inside the forest*. |
|  | I ran *into the forest*. |
|  | I ran *out of the forest*. |

Naturally, this applies for non-place destinations and sources as well.
However, these words don't have to be directly before a verb

| Ravna | English |
|-------|---------|
|  | She gave *me* an apple. |
|  | I fetched water *from the creek*. |

Instrumental adverbials behave just like normal adverbs:

| Ravna | English |
|-------|---------|
|  | He answered *in Ravna*. |
|  | They travel *on a boat*. |

Temporal adverbials and other adverbials, however, either follow
verbs:

| Ravna | English |
|-------|---------|
|  | Reva gets up *at 6* *every day*. |
|  | He has studied this topic *since 5 years ago*. |
|  | Ze worked *until midnight*. |
|  | He went to the river *to fish*. |
|  | Would you do that *for me*? |

## Exceptions

In a literary setting, especially in poetry, one can, and often breaks the rule
of word order to either shift emphasis or for rhyming.


