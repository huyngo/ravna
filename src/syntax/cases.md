# Cases

As we have learnt in [Nominal morphology][nom-morph], there are 4 cases:

- nominative: used for topic and subject of the sentence.
- accusative: used for direct object of the sentence
- dative: used for indirect objects of the sentence, as well as anything after
    a preposition
- genitive: signifying possession.

[nom-morph]: /morph/nom.md
