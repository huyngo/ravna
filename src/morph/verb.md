# Verbal

Verbs conjugation are based on tense, subject, and time in day.

There are three verb tenses: past, present, and future.  Aspects and mood are
separately shown on adverbs, which can often be omitted.  Verbs conjugate the
same for present tense throughout the day.

In the following subsection, conjugations are grouped by the pronouns:

- <b><i>te</i></b> and <b><i>mi</i></b> each has its own conjugation
- <b><i>vo</i></b>: <i>vo</i>, <i>yo</i> and singular animate nouns
- <b><i>yat</i></b>: <i>yat</i> and singular inanimate nouns
- <b><i>mite</i></b>: <i>seté</i>, <i>mite</i> and <i>chomí</i>
- <b><i>chovö</i></b>: <i>chovö</i> and plural animate nouns
- <b><i>boyät</i></b>: <i>boyät</i> and plural inanimate nouns

When conjugate for singular subject, the first non-prefix vowel is umlauted if
possible.

## Infinitive

Infinitive verbs in Ravna have 3 possible endings:

- <b><i>-yan</i></b>: the "default" ending—as most verbs end in <i>-yan</i>,
    a new verb would also end in this.
- <b><i>-vet</i></b>: much fewer verbs has this ending, but all verbs having
    this ending are regular.
- <b><i>-nis</i></b>: only auxiliary verbs end in this, which are irregular,
    but it share some patterns with the other two endings.  Time of the day
    also doesn't factor in how they are conjugated.

## Morning

Morning is the time from sunrise to noon.  A verb describing action starting in
the morning is conjugated like below.

Example 1: <i>fiyan</i> (to live)

| pronoun | present | past   | future |
|---------|---------|--------|--------|
| te      | fi      | fiya   | fiyo   |
| mi      | fid     | fida   | fiyod  |
| vo      | fiz     | fiza   | fiyoz  |
| yat     | fir     | fira   | fiyor  |
| mite    | fiyon   | fiyona | fiyonq |
| chovö   | fison   | fisona | fisonq |
| boyät   | filon   | filona | filonq |

Example 2: <i>karvet</i> (to shine)

| pronoun | present | past   | future |
|---------|---------|--------|--------|
| te      | kärvos  | kärvo  | kärvon |
| mi      | kärtos  | kärto  | kärton |
| vo      | kärzos  | kärzo  | kärzon |
| yat     | kärros  | kärro  | kärron |
| mite    | karvom  | karova | karvas |
| chovö   | karsom  | karosa | karsas |
| boyät   | karlom  | karola | karlas |

Example 3: <i>ponis</i> (to be able, can)

| pronoun | present | past   | future |
|---------|---------|--------|--------|
| te      | pö      | pöra   | pöron  |
| mi      | pet     | peta   | petod  |
| vo      | pas     | pasa   | pasoy  |
| yat     | pir     | pira   | piror  |
| mite    | pöron   | pörona | pöronq |
| chovö   | pason   | pasona | pasonq |
| boyät   | paron   | parona | paronq |

## Afternoon

Example 1: <i>fiyan</i> (to live)

| pronoun | past   | future |
|---------|--------|--------|
| te      | finey  | fiye   |
| mi      | fined  | fiyed  |
| vo      | finez  | fiyez  |
| yat     | finer  | fiyer  |
| mite    | fiyone | fiyenq |
| chovö   | fisone | fisenq |
| boyät   | firone | firenq |

Example 2: <i>karvet</i> (to shine)

| pronoun | past   | future |
|---------|--------|--------|
| te      | kärve  | kärven |
| mi      | kärte  | kärten |
| vo      | kärze  | kärzen |
| yat     | kärre  | kärren |
| mite    | karove | karves |
| chovö   | karose | karses |
| boyät   | karole | karles |

## Night

Example 1: <i>fiyan</i> (to live)

| pronoun | past   | future |
|---------|--------|--------|
| te      | fin    | finni  |
| mi      | findi  | fiyid  |
| vo      | finzi  | fiyiz  |
| yat     | finri  | fiyir  |
| mite    | fiyne  | fiyinq |
| chovö   | fisne  | fisinq |
| boyät   | firne  | firinq |

Example 2: <i>karvet</i> (to shine)

| pronoun | past   | future |
|---------|--------|--------|
| te      | kärvi  | kärvin |
| mi      | kärti  | kärtin |
| vo      | kärzi  | kärzin |
| yat     | kärri  | kärrin |
| mite    | karovi | karvis |
| chovö   | karosi | karsis |
| boyät   | karoli | karlis |
