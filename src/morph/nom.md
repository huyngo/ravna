# Nominal

Ravna nouns have two genders: animate and inanimate.  Animate nouns usually end
in vowels, while inanimate nouns often end in consonants.  Proper names are
obviously exceptions.
Genders of the nouns also inflects on articles, numbers, verbs, and adjectives
related to the noun.

There are four cases:

- nominative: used for topic and subject of the sentence.
- accusative: used for direct object of the sentence
- dative: used for indirect objects of the sentence, as well as anything after
    a preposition
- genitive: signifying possession.

The inflections distinguish between singular (0 or 1), paucal (2 to 10), and
plural (more or unknown).  In paucal and plural nouns, the first vowel is
umlauted if possible.

Some demonstratives has nominal morphology.

## Animate noun

Example word: <i>ravi</i> (root)

| case | singular | paucal | plural |
|------|----------|--------|--------|
| nom  | ravi     | rävine | rävno  |
| acc  | ravih    | rävize | rävzo  |
| dat  | ravik    | rävite | rävto  |
| gen  | ravil    | rävire | rävro  |

## Inanimate nouns

Example word: <i>nec</i> (rock)

| case | singular | paucal | plural |
|------|----------|--------|--------|
| nom  | nec      | necis  | necsim |
| acc  | necos    | necos  | necsom |
| dat  | necim    | necmis | necmis |
| gen  | necar    | necay  | necyar |
