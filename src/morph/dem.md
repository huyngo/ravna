# Demonstratives

|            | proximal | medial | distal | interogative | negative |
|------------|----------|--------|--------|--------------|----------|
| person[^1] | siri     | gori   | harri  | koni         | feli     |
| thing      | siris    | goris  | harris | konis        | felis    |
| place      | sihim    | gohim  | hahim  | konhim       | felim    |
| time       | simik    | gomik  | hammik | kommik       | felmik   |
| manner     | sitis    | gotis  | hattis | kontis       | feltis   |
| reason     | sinira   | gonira | hanira | konnira      | fennira  |

Demonstratives for person and thing have nominal morphology.
So are those for place and time, but they only have dative declension.
Demonstratives for manner and reason have adjectival morphology.

[^1]: Including humans and animate things
