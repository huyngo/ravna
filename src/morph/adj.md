# Adjectival

Adjectives conjugate similar to singular animate nouns and paucal inanimate
nouns.

Example word: <i>la</i> (big, great)

| case | animate | inanimate |
|------|---------|-----------|
| nom  | la      | lais      |
| acc  | lah     | laos      |
| dat  | lak     | laim      |
| gen  | lar     | lay       |

Articles have adjectival morphology:

| case | animate | inanimate |
|------|---------|-----------|
| nom  | na      | cis       |
| acc  | nah     | cos       |
| dat  | nak     | cim       |
| gen  | nar     | cay       |

Some demonstratives also have adjectival morphology.
