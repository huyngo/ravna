# Pronoun

Pronouns have similar conjugations to nouns, but with slightly more irregular
morphology.  It also doesn't distinguish between paucal and plural (but the
distinction still shows on verbs and adjectives it inflects)

## Singular

| case | I    | thou | ze (an.) | it (inan.) | generic |
|------|------|------|----------|------------|---------|
| nom  | te   | mi   | vo       | yat        | yo      |
| acc  | tena | mina | voma     | yam        | yona    |
| dat  | teta | mita | voca     | yak        | yota    |
| gen  | tera | mira | vora     | yoor       | yera    |

ze: singular genderless third-person pronoun used to avoid inconvenience of
"he/she" as well as the ambiguity of singular "they".

## Plural

| case | we/ex | we/in | you    | they (an.) | they (inan.) |
|------|-------|-------|--------|------------|--------------|
| nom  | seté  | mite  | chomí  | chovö      | boyät        |
| acc  | setén | miten | chomín | chovöm     | boyäm        |
| dat  | setét | mitet | chomít | chovöc     | boyäk        |
| gen  | setér | miter | chomír | chovör     | boyör        |

Note:

Prefix <i>se-</i> denotes humility and <i>cho-</i> denotes respect, so when
referring to friends or younger family members or children, using se- is more
common.  On the other hand, only chieftains and oldest member of a tribe can
use <i>cho-</i> to refer to self (i.e. <i>choté</i>), but they're rarely
together to use it.

Prefix bo- is almost only used for inanimate pronouns and nouns, but can also
be used for someone one really despises, like an enemy.
